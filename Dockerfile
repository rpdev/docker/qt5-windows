FROM fedora:rawhide

ARG WINARCH=mingw64
ARG WINARCH_NSIS=mingw32

RUN \
\
    dnf install -y --nogpgcheck \
        cmake \
        findutils \
        make \
        ninja-build \
        python \
        python2 \
        git \
        wget \
        ccache \
        \
        qt5-qtxmlpatterns-devel \
        qt5-linguist \
        appstream \
        \
        ${WINARCH}-qt5-qmake \
        ${WINARCH}-qt5-qmldevtools \
        ${WINARCH}-qt5-qmldevtools-devel \
        ${WINARCH}-qt5-qt3d \
        ${WINARCH}-qt5-qt3d-tools \
        ${WINARCH}-qt5-qtactiveqt \
        ${WINARCH}-qt5-qtbase \
        ${WINARCH}-qt5-qtbase-devel \
        ${WINARCH}-qt5-qtcharts \
        ${WINARCH}-qt5-qtdeclarative \
        ${WINARCH}-qt5-qtgraphicaleffects \
        ${WINARCH}-qt5-qtimageformats \
        ${WINARCH}-qt5-qtlocation \
        ${WINARCH}-qt5-qtmultimedia \
        ${WINARCH}-qt5-qtquickcontrols \
        ${WINARCH}-qt5-qtsensors \
        ${WINARCH}-qt5-qtserialport \
        ${WINARCH}-qt5-qtsvg \
        ${WINARCH}-qt5-qttools \
        ${WINARCH}-qt5-qttools-tools \
        ${WINARCH}-qt5-qttranslations \
        ${WINARCH}-qt5-qtwebkit \
        ${WINARCH}-qt5-qtwebsockets \
        ${WINARCH}-qt5-qtwinextras \
        ${WINARCH}-qt5-qtxmlpatterns \
        ${WINARCH}-angleproject \
        ${WINARCH}-gcc-c++ \
        ${WINARCH_NSIS}-nsis \
        ${WINARCH_NSIS}-nsiswrapper \
        \
        && \
        cd / && \
        git clone https://github.com/qt/qtquickcontrols2.git && \
        cd qtquickcontrols2 && \
        git checkout v$(${WINARCH}-qmake-qt5 -query QT_VERSION)-lts-lgpl && \
        ${WINARCH}-qmake-qt5 && \
        make -j4 && \
        make install && \
        cd .. && \
        rm -rf qtquickcontrols2 \
        && \
        cd / && \
        git clone https://github.com/qt/qtremoteobjects.git && \
        cd qtremoteobjects && \
        git checkout v$(${WINARCH}-qmake-qt5 -query QT_VERSION)-lts-lgpl && \
        ${WINARCH}-qmake-qt5 && \
        make -j4 && \
        make install && \
        cd .. && \
        rm -rf qtremoteobjects \
        && \
        cd / && \
        git clone https://github.com/qt/qtnetworkauth.git && \
        cd qtnetworkauth && \
        git checkout v$(${WINARCH}-qmake-qt5 -query QT_VERSION)-lts-lgpl && \
        ${WINARCH}-qmake-qt5 && \
        make -j4 && \
        make install && \
        cd .. && \
        rm -rf qtnetworkauth \
        